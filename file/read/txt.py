# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

from pathlib import Path


def txt_line_to_str(txt_path):
    """
    Reads a line of text from a file
    :param txt_path: the file to read
    :return: txt_string: the file as a string
    """
    with Path(txt_path).open(mode='r') as txt_file:
        txt_string = str(txt_file.read()).rstrip()
    return txt_string
