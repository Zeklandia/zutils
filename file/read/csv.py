# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

import csv
from pathlib import Path


def csv_to_list(csv_path):
    """
    Reads a CSV file and returns a list of objects.
    :param csv_path: the path of the CSV file
    :return: csv_list: a list of the contents of the CSV file
    """

    with Path(csv_path).open(mode='r') as csv_file:
        temp_reader = csv.reader(csv_file, quoting=csv.QUOTE_NONE)
        csv_list = [lc_item for lc_line in temp_reader for lc_item in lc_line]
    return csv_list


def csv_to_dict(csv_path):
    """
    Read a CSV file and return a dictionary. Intended for two-column, comma-delimited CSV files.
    :param csv_path: the CSV file to read
    :return: csv_dict: a dictionary of file_path's contents
    """

    with Path(csv_path).open(mode='r') as csv_file:
        temp_reader = csv.reader(csv_file)
        next(temp_reader)
        csv_dict = dict(temp_reader)
    return csv_dict
