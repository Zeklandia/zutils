# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

import csv
from pathlib import Path


def dict_to_csv(dict_var, csv_path, column_headers=["key", "value"]):
    """
    Write a Dict() to a CSV file.
    :param dict_var: dictionary to write
    :param csv_path: path of file to write to
    :param column_headers: (optional) headers to use in the first row
    :return: you get nothing
    """

    with Path(csv_path).open(mode='w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(column_headers)
        for loop_key in dict_var:
            writer.writerow(zip(loop_key, dict_var[loop_key]))
