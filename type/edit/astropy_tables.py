# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)


def fill_masked_table(table, fill=None):
    """
    Fill a masked astropy table with a given filler.
    :param table: astropy table to unmask
    :param fill: the filler
    :return: a filled table
    """

    # fill table if masked
    if table.masked:
        return table.filled(fill)
    else:
        return table


def remove_rows_remove_value(table, arg_column_name, list_values):
    """
    Remove rows not containing given values in a specific column.
    :param table: astropy table to strip
    :param arg_column_name: column of table to search
    :param list_values: list of values to remove
    :return: astropy table with only rows containing none of values_remove in column_name
    """

    table = table

    # remove unwanted bands
    for loop_value in list_values:
        table = table[table[arg_column_name] != loop_value]

    return table


def remove_rows_keep_value(table, column_name, list_values):
    """
    Remove rows not containing given values in a specific column.
    :param table: astropy table to strip
    :param column_name: column of table to search
    :param list_values: list of values to keep
    :return: astropy table with only rows containing one of values_keep in column_name
    """

    import numpy as np

    # identify unwanted bands
    temp_values_remove = [lc_value for lc_value in np.unique(table[column_name]) if
                          lc_value not in list_values]

    # remove unwanted bands
    for loop_value in temp_values_remove:
        table = table[table[column_name] != loop_value]

    return table


def replace_value_in_table(table, column_name, cipher):
    """
    Find and replace values in a column of a table.
    :param table: astropy table
    :param column_name: name of column in table to search
    :param cipher: dictionary with what to find as keys and what to replace them with as values
    :return: astropy table with values in column_name replaced
    """

    # replace values in table
    table[column_name] = [cipher[lc_value] for lc_value in table[column_name]]

    return table
